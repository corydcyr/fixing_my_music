#!/usr/bin/env

import os


MUSIC_ROOT="/home/cory/Music/amazon_music"
COMP_FOLDER = "Compilations"

with open("compilation_tracks.txt") as in_f:
    for line in in_f:
        album, artist = line.strip().split("\t")
        if artist == COMP_FOLDER:
            continue
        src_path = os.path.join(MUSIC_ROOT, artist, album)
        dest_path = os.path.join(MUSIC_ROOT, COMP_FOLDER, album)
        try:
            os.mkdir(dest_path)
        except OSError:
            pass

        for src_file in os.listdir(src_path):
            os.rename(os.path.join(src_path, src_file), os.path.join(dest_path, src_file))
            

