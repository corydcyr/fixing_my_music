#!/usr/bin/env python

# input: list of files relative to root music directory
# output: tsv of [ALBUM, ARTIST, FILE]
with open("file_list.txt") as in_f:
    with open("files_by_album.txt",'w') as out_f:
        for line in in_f:
            parts = line.strip().split('/')
            out_f.write("%s\t%s\t%s" % (parts[1], parts[0], parts[2]))
            out_f.write("\n")

