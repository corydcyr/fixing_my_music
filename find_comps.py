#!/usr/bin/env python 
import collections

dat = collections.defaultdict(set)

with open("files_by_album.txt") as in_f:
    for line in in_f:
        parts = line.strip().split("\t")
        dat[parts[0]].add(parts[1])

with open("compilation_tracks.txt", 'w') as out_f:
    for album, artists in dat.iteritems():
        if len(artists) > 1:
            for artist in artists:
                out_f.write("%s\t%s" % (album, artist))
                out_f.write("\n")

